import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3 as Ubuntu
import Morph.Web 0.1
import QtWebEngine 1.7
import QtSystemInfo 5.5

import "Components"

ApplicationWindow {
    id: root
    visible: true
    color: '#1B1A26'

    width: units.gu(45)
    height: units.gu(75)

    property bool landscape: width > height
    onLandscapeChanged: {
        console.log('landscape');
        console.log(landscape);
    }

    WebContext {
        id: webcontext
        userAgent: 'Mozilla/5.0 (Linux; Ubuntu 16.04) AppleWebKit/537.36 Chrome/65.0.3325.151 Safari/537.36'

        userScripts: [
            WebEngineScript {
                id: cssinjection
                injectionPoint: WebEngineScript.DocumentCreation
                sourceUrl: Qt.resolvedUrl('inject.js')
                worldId: WebEngineScript.MainWorld
            }
        ]
    }

    WebView {
        id: webview
        anchors {
            top: parent.top
            left: (isFullScreen || !landscape) ? parent.left : navBar.right
            right: parent.right
            bottom: (isFullScreen || landscape) ? parent.bottom : navBar.top
        }

        // TODO error page
        context: webcontext
        url: 'https://vrv.co/'

        onUrlChanged: navBar.url = url

        onFullScreenRequested: function(request) {
            if (request.toggleOn) {
                root.showFullScreen();
            }
            else {
                root.showNormal();
            }
            request.accept();
        }

        function navigationRequestedDelegate(request) {
            var url = request.url.toString();
            var isvalid = false;

            if (!url.match('(http|https)://vrv.co/(.*)') && request.isMainFrame) {
                Qt.openUrlExternally(url);
                request.action = WebEngineNavigationRequest.IgnoreRequest;
            }
        }
    }

    NavBar {
        height: landscape ? parent.width : units.gu(6)
        width: landscape ? units.gu(6) : parent.width

        id: navBar
        visible: !webview.isFullScreen
        anchors {
            top: landscape ? parent.top : undefined
            left: parent.left
            right: landscape ? undefined : parent.right
            bottom: parent.bottom
        }

        landscape: root.landscape
        onUrlChanged: webview.url = url
    }

    ScreenSaver {
        screenSaverEnabled: !Qt.application.active || !webview.recentlyAudible
    }

    Connections {
        target: Ubuntu.UriHandler
        onOpened: {
            webview.url = uris[0];
        }
    }

    Component.onCompleted: {
        if (Qt.application.arguments[1] && Qt.application.arguments[1].indexOf('vrv.co') >= 0) {
            webview.url = Qt.application.arguments[1];
        }
    }
}
