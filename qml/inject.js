function hashChange() {
    if (window.location.hash == '#channels') {
        document.querySelector('.nav-item-clickable').click()
    }
    else if (window.location.hash == '#search') {
        var actionItems = document.querySelectorAll('.user-actions-item');
        actionItems[actionItems.length - 2].querySelector('div').click()
    }
}


window.onhashchange = hashChange;
window.addEventListener('load', hashChange, false);